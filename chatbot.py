#!/usr/bin/env python
# -*- coding: utf-8 -*-

from telegram.ext import Updater, CommandHandler, MessageHandler, Filters


class Chatbot():
    "Our chatbot"

    def understand(self, message):
        pass

    def generate(self):
        return "Ничего не понял"

    def process(self, message):
        self.understand(message) 
        return self.generate()


    def process_tg(self, bot, update):
        update.message.reply_text(self.process(update.message.text))

    def error(self, bot, update, error):
        print('Update "%s" caused error "%s"' % (update, error))

    def start_tg(self,bot, update):
        update.message.reply_text(self. generate())

    def run(self, tg=True):
        """Start the bot."""
        if tg:
            updater = Updater("509994173:AAHAx11NYi9-pXLIbULkc5YxADZnf4w69ZI")
            dp = updater.dispatcher
            dp.add_handler(CommandHandler("start", self.start_tg))
            dp.add_handler(MessageHandler(Filters.text, self.process_tg))

            dp.add_error_handler(self.error)
            updater.start_polling()

            print("Tg started")

            updater.idle()
        while True:
            print(self.generate())
            self.understand(input().strip())



if __name__ == '__main__':
    chat = Chatbot()
    chat.run(True)
